﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class DropCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            if (userInput.Arguments.Count == 0)
            {
                return new CommandResponse("If you want to drop something you need to tell me what you want to drop");
            }

            String pickUpItem = (String)userInput.Arguments[0];
            if (thePlayer.CurrentLocation is Shop)
            {
                return new CommandResponse("You need to sell on the shop you can't drop the item");
            }
            else
            {
                if (thePlayer.ContainsCarryingItem(pickUpItem))
                {
                    thePlayer.CurrentLocation.Items.AddItem(thePlayer.getCarryingItemByLabel(pickUpItem));
                    thePlayer.DropItem(thePlayer.getCarryingItemByLabel(pickUpItem));
                    return new CommandResponse("You successfully dropped the item " + pickUpItem);
                }
                else
                    return new CommandResponse("You are not successfull to drop the item " + pickUpItem);
            }
        }
    }
}