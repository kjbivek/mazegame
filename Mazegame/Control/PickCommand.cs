﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class PickCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            if (userInput.Arguments.Count == 0)
            {
                return new CommandResponse("If you want to pick something you need to tell me what you want to pickup");
            }

            String pickUpItem = (String)userInput.Arguments[0];
            if (thePlayer.CurrentLocation is Shop)
            {
                return new CommandResponse("You need to buy on the shop you can't pick up the item");
            }
            else { 
                if (thePlayer.CurrentLocation.Items.ContainsItem(pickUpItem))
                {
                    if (thePlayer.AddItem(thePlayer.CurrentLocation.Items.getItemByLabel(pickUpItem)))
                    {
                        thePlayer.CurrentLocation.Items.RemoveItem(pickUpItem);
                        return new CommandResponse("You successfully picked up " + pickUpItem + thePlayer.getCarryingItemByLabel(pickUpItem).ToString());


                    }

                    else
                        return new CommandResponse("Can't pickup either exceeded weight limit or already have the item");
                }
                else
                    return new CommandResponse("Sorry, you are not successfull to pick up " + pickUpItem);
            }
        }
    }
}