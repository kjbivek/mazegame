﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class ShowCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            if (userInput.Arguments.Count == 0)
            {
                return new CommandResponse("There is only one argumnet available for this command i.e. inventory");
            }

            String inventory = (String)userInput.Arguments[0];
            if (inventory.Equals("inventory"))
            {
                thePlayer.showInventories();
                return new CommandResponse("Successfully shown the inventories");
            }
            
            else
                return new CommandResponse("Can't show the inventories");
        }
    }
}