﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class CommandHandler
    {
        public static ArrayList commands = new ArrayList();
        private Hashtable availableCommands;
        private Parser theParser;

        public CommandHandler()
        {
            availableCommands = new Hashtable();
            SetupCommands();
            theParser = new Parser(new ArrayList(availableCommands.Keys));
        }

        private void SetupCommands()
        {
            availableCommands.Add("go", new MoveCommand());
            commands.Add("go");
            availableCommands.Add("quit", new QuitCommand());
            commands.Add("quit");
            availableCommands.Add("move", new MoveCommand());
            commands.Add("move");
            availableCommands.Add("look", new LookCommand());
            commands.Add("look");
            availableCommands.Add("pick", new PickCommand());
            commands.Add("pick");
            availableCommands.Add("drop", new DropCommand());
            commands.Add("drop");
            availableCommands.Add("show", new ShowCommand());
            commands.Add("show");
            availableCommands.Add("buy", new BuyCommand());
            commands.Add("buy");
            availableCommands.Add("sell", new SellCommand());
            commands.Add("sell");
        }

        public CommandResponse ProcessTurn(String userInput, Player thePlayer)
        {
            ParsedInput validInput = theParser.Parse(userInput);
            Command theCommand = (Command) availableCommands[validInput.Command];
            if (theCommand != null) {
                return theCommand.Execute(validInput, thePlayer);
            }
            else
            {
                return new CommandResponse("Not a valid command");
            }
        }
    }
}
