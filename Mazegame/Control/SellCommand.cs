﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class SellCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            if (userInput.Arguments.Count == 0)
            {
                return new CommandResponse("If you want to sell something you need to tell me what you want to sell");
            }

            String sellItem = (String)userInput.Arguments[0];
            if (thePlayer.CurrentLocation is Shop)
            {
                if (thePlayer.ContainsCarryingItem(sellItem))
                {
                    thePlayer.CurrentLocation.Items.AddItem(thePlayer.getCarryingItemByLabel(sellItem));
                    thePlayer.Money.Add(thePlayer.getCarryingItemByLabel(sellItem).Worth);
                    thePlayer.DropItem(thePlayer.getCarryingItemByLabel(sellItem));
                    return new CommandResponse("You successfully sold the item " + sellItem+"\n You sold it for: "+ thePlayer.CurrentLocation.Items.getItemByLabel(sellItem).Worth+"\n The total amount you have is: "+thePlayer.Money.getAmount());
                }
                else
                    return new CommandResponse("You are not successfull to sell the item " + sellItem);
                
            }
            else
            {
                return new CommandResponse("You only need to sell on the shop this is not a shop");
            }
        }
    }
}