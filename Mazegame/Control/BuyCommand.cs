﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class BuyCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            if (userInput.Arguments.Count == 0)
            {
                return new CommandResponse("If you want to buy something you need to tell me what you want to buy");
            }

            String itemToBuy = (String)userInput.Arguments[0];
            if (thePlayer.CurrentLocation is Shop)
            {
                if (thePlayer.Money.getAmount() >= thePlayer.CurrentLocation.Items.getItemByLabel(itemToBuy).Worth)
                {
                    if (thePlayer.CurrentLocation.Items.ContainsItem(itemToBuy))
                    {
                        if (thePlayer.AddItem(thePlayer.CurrentLocation.Items.getItemByLabel(itemToBuy)))
                        {
                            thePlayer.Money.Subtract(thePlayer.CurrentLocation.Items.getItemByLabel(itemToBuy).Worth);
                            thePlayer.CurrentLocation.Items.RemoveItem(itemToBuy);
                            return new CommandResponse("You successfully bought " + itemToBuy + thePlayer.getCarryingItemByLabel(itemToBuy).ToString());


                        }

                        else
                            return new CommandResponse("Can't buy the item. Either exceeded weight limit or already have the item");
                    }
                    else
                        return new CommandResponse("Sorry, you are not successfull to buy " + itemToBuy);
                }
                else
                {
                    return new CommandResponse("Sorry, you don't have enough amount of money to buy \n Amount you have: "+thePlayer.Money.getAmount()+"\n Cost of item: "+thePlayer.CurrentLocation.Items.getItemByLabel(itemToBuy).Worth);
                }
                
            }
            else
            {
                return new CommandResponse("You only need to buy on the shop. This is not a shop");
            }
        }
    }
}